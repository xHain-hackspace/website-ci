---
title: Einladung zum Gespräch auf der Lichtung am 04.12.2022 
image: /images/articles/gespraechunterbaeumen.jpg
image_reference: ""
date: 2022-11-24
categories:
- news
- events
tags:
- xhain
type: blog
---

Liebe Gesprächsfreundinnen und -freunde!

Wir möchten euch herzlich zum zweiten und letzten Gespräch auf der Lichtung am 04.12.2022 im xHain einladen. Das Gespräch auf der Lichtung ist ein Salon, in dem wir die Auswirkungen der Pandemie auf unsere Gesprächskultur reflektieren. Insbesondere wollen wir festhalten, was wir an Gutem behalten und für unsere Gesellschaft kultivieren möchten.

Das Gespräch auf der Lichtung findet von 11.00 bis 18.00 Uhr im xHain (Grünberger Str. 16, 10243 Berlin) statt. Der Termin baut auf dem ersten Gespräch auf der Lichtung vom 20.11. auf, ist aber absolut ohne Vorwissen besuchbar. Das einzige Vorwissen, das ihr braucht, ist, in dieser Pandemie zu leben. 

Wir haben eine sehr gut funktionierende Hybridlösung erarbeitet - deshalb können wir euch auch die Teilnahme von zu Hause aus empfehlen, wenn ihr nicht nach Berlin kommen wollt oder könnt.

Insbesondere wollen wir uns am 04.12. darüber unterhalten:
1. Gesprächsregeln und -sanktionen: Wie gibt man Feedback?
2. Wie bleibt man im Gespräch unter widrig(st)en Bedingungen?

[Hier haben wir dokumentiert](https://wiki.x-hain.de/en/Events/Gespr%C3%A4ch-auf-der-Lichtung), welche Unterhaltungen im Rahmen des Gesprächs auf der Lichtung bereits stattgefunden haben.

Hier geht es zur [Anmeldung.](https://files.x-hain.de/apps/forms/YCooT9AfZCFcWSz7)

Wir sind eine Gruppe aus dem xHain, die aus dem [Gespräch unter Bäumen](https://wiki.x-hain.de/de/Events/Gespr%C3%A4chunterB%C3%A4umen) hervor gegangen ist. Zur Gruppe gehören u.a. Maria Reimer, [Gregor Sedlag](https://twitter.com/gregorsedlag) und [Marcus Richter](https://twitter.com/monoxyd). 

Dank der Fördergelder des Sonderprogramms [Neustart Kultur](https://www.fonds-soziokultur.de/gefoerderte-projekte/sonderprogramm-neustart-kultur.html) des [Fonds Soziokultur](https://www.fonds-soziokultur.de/) ist die Teilnahme an den Gesprächen auf der Lichtung kostenlos. In begrenztem Rahmen können wir Reisekosten erstatten.

Vermutlich werdet ihr euch beim Gespräch auf der Lichtung wohl fühlen, wenn euch diese Fragen interessieren: 

*Was ist ein gutes Gespräch? Was ist ein gutes Gespräch im soziokulturellen Raum? Wie soll deine Community beschaffen sein, um gute Gespräche zu ermöglichen? Was brauchen gute Gespräche? Was brauchen Gespräche, um gut zu sein?*

*Wie hat sich deine Community seit dem Beginn der Pandemie verändert? Wie hast du dich verändert? Was daran ist gut?*

*Was muss mehr werden in einer Dauerkrisenumgebung? Was muss im Inneren mehr werden, wenn Außen Dauerkrise ist?*

*Wie bleiben wir humor- und lustvoll, ohne zynisch oder entkoppelt zu sein?*

*Was machen wir, wenn wir uns auf einer Lichtung begegnen? Was machst du, wenn du deiner Freundin auf einer Lichtung begegnest? Was machst du, wenn du deiner Feindin auf einer Lichtung begegnest?*

Das **Gespräch auf der Lichtung** ist offen für alle. Egal, ob ihr noch nie einen Hackspace betreten habt oder Member im xHain seid. 

P.S.: Wir akzeptieren nur Anmeldungen von Menschen mit vollständigem Impfschutz, die sich zusätzlich täglich testen. Die Infrastruktur hierfür stellen wir bereit.

Wir sind sehr glücklich über die Förderung und danken:

![Hier stehen die Logos der Förderer](/images/articles/logopaket3.png)

